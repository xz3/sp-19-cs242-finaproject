package controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.List;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import gui.GUI;
import game.Platform;
import game.Components;
import javafx.util.Pair;

import static javax.swing.SwingUtilities.isLeftMouseButton;


public class Controller {
	private Platform platform;
	private GUI gui; 
	private int score;
	private JButton swi;
	private int swiX;
	private int swiY;
	int dir;
	private Components curComponents;
	private int countRound;
	
    private Timer timerBall = null;
    private Timer timerBomb = null;
    private Timer timerStar = null;
    private Timer timerCapsuleB = null;
    private Timer timerCapsuleS = null;
    private Timer timerTimer = null;
    private Timer count = null;
	

	public Controller(){
		platform = new Platform();
		
		dir = platform.getDir();
		
		score = 0;
		
		countRound = 0;
		
		gui = new GUI();
		
		gui.newGame();
		
		count = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	if(gui.isStopped() == 1) {
            		gui.timer.stop();
            		gui.timer.stop();
            		timerBomb.stop();
            		timerStar.stop();
            		timerCapsuleB.stop();
            		timerCapsuleS.stop();
            		timerBall.stop();
            		count.stop();
					JOptionPane.showMessageDialog(gui.getFrame(), "Game Ends!",
							"Game Ends!", JOptionPane.ERROR_MESSAGE);
					restart();
            	}
            }
        });
		
		timerBomb = new Timer(5000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	int curX = platform.getBombX();
            	int curY = platform.getBombY();
                platform.removeBomb();
        		Random rand = new Random(); 
        		int x = rand.nextInt(16);
        		int y = rand.nextInt(16);
        		while((x+2) == platform.getStarX() && (y+2) == platform.getStarY()) {
        			x = rand.nextInt(16);
	        		y = rand.nextInt(16);
        		}
        		platform.addBomb(x+2,y+2);
        		gui.updateImage(platform, x+2, y+2);
        		gui.updateImage(platform, curX, curY);
            }
        });
    	
    	timerStar = new Timer(5000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	int curX = platform.getStarX();
            	int curY = platform.getStarY();
                platform.removeStar();
        		Random rand = new Random(); 
        		int x = rand.nextInt(16);
        		int y = rand.nextInt(16);
        		while((x+2) == platform.getBombX() && (y+2) == platform.getBombY()) {
        			x = rand.nextInt(16);
	        		y = rand.nextInt(16);
        		}
        		platform.addStar(x+2,y+2);
        		gui.updateImage(platform, x+2, y+2);
        		gui.updateImage(platform, curX, curY);
            }
        });
    	
    	timerBall = new Timer(200 * Math.abs(dir), new ActionListener() {
            public void actionPerformed(ActionEvent e) {
        		platform.isBigger();
        		platform.isSmaller();
            	if(countRound == 10) platform.sizeNormal();
            	countRound += 1;
            	dir = platform.getDir();
            	int curX = platform.getBallX();
            	int curY = platform.getBallY();
            	int desX = curX + (dir/Math.abs(dir));
            	int desY = curY - dir;
            	System.out.println(desX+","+desY+" dir is "+dir);
            	Components ball = platform.getComponent(curX, curY);
				
				if(platform.isHit()) {
					System.out.println("ishit");
					score++;
					gui.setScore(score);
					platform.changeDir();
					dir = platform.getDir();
	            	desX = curX + (dir/Math.abs(dir));
	            	desY = curY - dir;
	            	platform.moveBall(desX, desY);
					gui.updateImage(platform, curX,curY);
					gui.updateImage(platform, desX, desY);
				}
				else if(platform.doesLose()) {
            		gui.timer.stop();
            		gui.timer.stop();
            		timerBomb.stop();
            		timerStar.stop();
            		timerCapsuleB.stop();
            		timerCapsuleS.stop();
            		timerBall.stop();
            		count.stop();
					JOptionPane.showMessageDialog(gui.getFrame(), "Game Ends!",
							"Game Ends!", JOptionPane.ERROR_MESSAGE);
					restart();
				}
				else if(ball.isPossible(desX, desY)){
					platform.moveBall(desX, desY);
					gui.updateImage(platform, curX,curY);
					gui.updateImage(platform, desX, desY);
				}
				else {
					platform.moveBall(0, 0);
					gui.updateImage(platform, curX,curY);
            		gui.timer.stop();
            		gui.timer.stop();
            		timerBomb.stop();
            		timerStar.stop();
            		timerCapsuleB.stop();
            		timerCapsuleS.stop();
            		timerBall.stop();
            		count.stop();
					JOptionPane.showMessageDialog(gui.getFrame(), "Game Ends!",
							"Game Ends!", JOptionPane.ERROR_MESSAGE);
					restart();
				}
				
				if(platform.doesScore()) {
					score++;
					gui.setScore(score);
				}
				
				if(platform.incTime()) {
					gui.addTime(10000);
				}
				
				if(platform.isBig() || platform.isSmall()) {
					System.out.println("turning");
					countRound = 0;
				}
				
            }
        });
		
    	timerCapsuleB = new Timer(5000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	int curX = platform.getCapBX();
            	int curY = platform.getCapBY();
                platform.removeCapB();
        		Random rand = new Random(); 
        		int x = 18;
        		int y = rand.nextInt(18);
        		platform.addCapB(x,y+1);
        		gui.updateImage(platform, 18, y+1);
        		gui.updateImage(platform, curX, curY);
            }
        });
    	
    	timerCapsuleS = new Timer(10000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	int curX = platform.getCapSX();
            	int curY = platform.getCapSY();
                platform.removeCapS();
        		Random rand = new Random(); 
        		int x = 1;
        		int y = rand.nextInt(18);
        		platform.addCapS(x,y+1);
        		gui.updateImage(platform, 1, y+1);
        		gui.updateImage(platform, curX, curY);
            }
        });
    	
    	timerTimer = new Timer(5000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	int curX = platform.getTimerX();
            	int curY = platform.getTimerY();
                platform.removeTimer();
        		Random rand = new Random(); 
        		int x = rand.nextInt(16);
        		int y = rand.nextInt(16);
        		while((x+2) == platform.getBombX() && (y+2) == platform.getBombY()) {
        			x = rand.nextInt(16);
	        		y = rand.nextInt(16);
        		}
        		platform.addTimer(x+2,y+2);
        		gui.updateImage(platform, x+2, y+2);
        		gui.updateImage(platform, curX, curY);
            }
        });
		
		JOptionPane.showMessageDialog(gui.getFrame(), "Now the game starts!", "Now the game starts!", JOptionPane.INFORMATION_MESSAGE);

		gameLoop();
	}
	
	public void newGame(){
		int response = JOptionPane.showConfirmDialog(null, "You do want to start a new game?", "You do want to start a new game?", JOptionPane.YES_NO_OPTION);
		if(response == JOptionPane.YES_OPTION){
			Controller.main(null);
		}
		else{
			JOptionPane.showMessageDialog(gui.getFrame(), "Back to the game now", "Back to the game now", JOptionPane.INFORMATION_MESSAGE);
			return;
		}
			
	}
	
	
	public void reset() {
		if(swi != null){
			swi = null;
		}
		gui.newGame();
		platform = new Platform();
		gui.timer.restart();
		gui.timer.restart();
		timerBomb.restart();
		timerStar.restart();
		timerCapsuleB.restart();
		timerCapsuleS.restart();
		timerBall.restart();
		count.restart();
	}
	
	

	
	
	public void restart(){
		int reply;
		reply = JOptionPane.showConfirmDialog(null,
				"Do you want to restart a game?", "Do you want to restart a game?", JOptionPane.YES_NO_OPTION);
		
		if(reply == JOptionPane.YES_OPTION){
			reset();
			score = 0;
			gui.setScore(score);
		}
	}
	
	
	
	/**
	 * This function is the main game loop.
	 *
	**/
	
	public void gameLoop(){
		
		gui.timer.start();
		timerBomb.start();
		timerStar.start();
		timerCapsuleB.start();
		timerCapsuleS.start();
		timerBall.start();
		count.start();
		

		for(int i = 0; i<20; i++){
			for(int j = 0; j<20; j++){
				final int I = i;
				final int J = j;
				JButton curr = gui.getTile(I, J);
				
				curr.addMouseListener(new MouseListener(){
					public void mouseClicked(final MouseEvent e){
						
						if(isLeftMouseButton(e)){
							swi = gui.getTile(I, J);
							swiX = I;
							swiY = J;
							int curXmin = platform.getSwitchXL();
							int curXmax = platform.getSwitchXR();
							int curYmin = platform.getSwitchYL();
							int curYmax = platform.getSwitchYR();
							if(platform.canMoveSwitch(swiX, swiY)) {
								for(int idx = curXmin; idx <= curXmax; idx++) {
									for(int idy = curYmin; idy <= curYmax; idy++) {
										gui.updateImage(platform, idx, idy);
									}
								}
								if(swiX == 0) {
									if(swiY <= 2) {
										setSwitch(0,0,0,2);
									}
									if(swiY >= 18) {
										setSwitch(0,0,17,19);
									}
									else {
										setSwitch(0,0,swiY-1,swiY+1);
									}
								}
								else if(swiX == 19) {
									if(swiY <= 2) {
										setSwitch(19,19,0,2);
									}
									if(swiY >= 17) {
										setSwitch(19,19,17,19);
									}
									else {
										setSwitch(19,19,swiY-1,swiY+1);
									}
								}
								else if(swiY == 0) {
									setSwitch(swiX-1,swiX+1,0,0);
								}
								else if(swiY == 19) {
									setSwitch(swiX-1,swiX+1,19,19);
								}
							}
							else {
								wrongMove();
							}
						}
						
					}

					private void setSwitch(int xStart, int xEnd, int yStart, int yEnd) {
						for(int i = xStart; i <= xEnd; i++ ) {
							for(int j = yStart; j <= yEnd; j++) {
								gui.updateImage(platform, i, j);
							}
						}
					}
					
					private void wrongMove() {
						swi = null;
						JOptionPane.showMessageDialog(gui.getFrame(), "You cannot move the switch here",
								"You cannot move the switch here!", JOptionPane.ERROR_MESSAGE);
					}
					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
				});
			}
		}
		
	}
	
	public static void main(String[] args) {
		Controller controller = new Controller();
	}
	


	
}
