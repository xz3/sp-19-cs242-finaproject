package game;


public class Components {

	public int xcoor, ycoor;
	private String type;

//	Constructor for Conponents
//	param@x: the x location for this component
//	param@y: the y location for this component
	
	public Components(int x, int y, String t) {
		xcoor = x;
		ycoor = y;
		type = t;
	}
	
//	Get the type of the component
	
	public String getType() {
		return this.type;
	}

	
	
// Return True if the position is possible for this component to be placed
	
	public boolean isPossible(int desX, int desY) {
		if(type == "Ball") {
			if(desX > 19 || desX < 0 || desY > 19 || desY < 0) return false;
			return true;
		}
		if(type == "Star" || type == "Bomb" || type == "Capsule" || type == "Timer") {
			if(desX < 19 && desX > 0 && desY < 19 && desY > 0) return true;
			return false;
		}
		if(type == "Switch") {
			if(desX == 0) return true;
			if(desY == 0) return true;
			if(desX == 19) return true;
			if(desY == 19) return true;
			return false;
		}
		return false;
	}
	
}
