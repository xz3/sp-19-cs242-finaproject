package game;
import java.util.Random;

public class Platform {

    private static final int HEIGHT = 20;
    private static final int WIDTH = 20;
    private static final int INCREMENT = 3;
    private Components[][] board;
    private int direction;
    private static int switchXL;
    private static int switchXR;
    private static int switchYL;
    private static int switchYR;
    private static int ballX;
    private static int ballY;
    private static int bombX;
    private static int bombY;
    private static int starX;
    private static int starY;
    private static int capBX;
    private static int capBY;
    private static int capSX;
    private static int capSY;
    private static int timerX;
    private static int timerY;
    private static int isSmall;
    private static int isBig;

	//initialize the platform
	
	public Platform() {
		board = new Components[WIDTH][HEIGHT];
		//setup the initial position for ball
		isSmall = 0;
		isBig = 0;
		switchXL = 0;
		switchXR = INCREMENT-1;
		switchYL = HEIGHT-1;
		switchYR = HEIGHT-1;
		ballX = WIDTH-2;
		ballY = 1;
		timerX = 17;
		timerY = 2;
		capBX = 15;
		capBY = 4;
		capSX = 4;
		capSY = 15;
		bombX = 13;
		bombY = 4;
		starX = 15;
		starY = 9;
		
		board[ballX][ballY] = new Components(ballX,ballY,"Ball");
		
		for(int i = 0; i < INCREMENT; i++) {
			board[i][HEIGHT-1] = new Components(i,HEIGHT-1,"Switch");
		}
		Random rand = new Random(); 
		direction = -1;
	}
	
	
	//add components to the board for testing purpose

	public void addComponent(int x, int y, String type) {
		if(type == "Ball") {
			board[x][y] = new Components(x,y,"Ball");
			ballX = x;
			ballY = y;
		}
		if(type == "Switch") {
			board[x][y] = new Components(x,y,"Switch");
			switchXL = x;
			switchXR = x;
			switchYL = y;
			switchYR = y;
		}
		if(type == "Bomb") {
			board[x][y] = new Components(x,y,"Bomb");
			bombX = x;
			bombY = y;
		}
	}
	
	//for new implemented components setup
	public void setBoard() {
		Random rand = new Random(); 
		board[WIDTH-2][1] = new Components(WIDTH-2,1,"Ball");
		ballX = WIDTH-2;
		ballY = 1;
		for(int i = 0; i < INCREMENT-1; i++) {
			board[i][HEIGHT-1] = new Components(i,HEIGHT-1,"Switch");
		}
		switchXL = 0;
		switchXR = INCREMENT-1;
		switchYL = HEIGHT-1;
		switchYR = HEIGHT-1;
		isSmall = 0;
		isBig = 0;
		
		direction = -rand.nextInt(4);
		if(direction == 0) direction = -1;
	}
	
	//check if there exists a piece in the given position
	
	public boolean existComponent(int x, int y) {
		return board[x][y] != null;
	}
	
	//remove piece in the given position and return the piece
	
	public Components removeComponent(int x, int y) {
		Components c = null;
		if(existComponent(x,y)) {
			c = board[x][y];
			board[x][y] = null;
		}
		return c;
	}
	
	
	// return direction of current ball
	public int getDir() {
		return direction;
	}

	// return position of current ball
	public int getSwitchXL() {
		return switchXL;	
	}
	
	public int getSwitchXR() {
		return switchXR;	
	}
	
	public int getSwitchYL() {
		return switchYL;	
	}
	
	public int getSwitchYR() {
		return switchYR;	
	}
	
	// return position of current ball
	public int getBallX() {
		return ballX;	
	}
		
	public int getBallY() {
		return ballY;	
	}
	
	// return position of current star
	public int getStarX() {
		return starX;	
	}
	
	public int getStarY() {
		return starY;	
	}
	
	// return position of current bomb
	public int getBombX() {
		return bombX;	
	}
		
	public int getBombY() {
		return bombY;	
	}
	
	// return position of current capsuleBig
	public int getCapBX() {
		return capBX;	
	}
		
	public int getCapBY() {
		return capBY;	
	}
	
	// return position of current capsuleSmall
	public int getCapSX() {
		return capSX;	
	}
		
	public int getCapSY() {
		return capSY;	
	}
	
	// return position of current timer
	public int getTimerX() {
		return timerX;	
	}
			
	public int getTimerY() {
		return timerY;	
	}
	
	//move the switch to destination position and return if it is successful
	
	public boolean canMoveSwitch(int desX, int desY) {
		Components s = new Components(desX,desY,"Switch");
		if(s.isPossible(desX,desY)) {
			if(desX == 0) {
				if(desY <= 2) {
					setSwitch(0,0,0,2);
				}
				if(desY >= 18) {
					setSwitch(0,0,17,19);
				}
				else {
					setSwitch(0,0,desY-1,desY+1);
				}
			}
			else if(desX == 19) {
				if(desY <= 2) {
					setSwitch(19,19,0,2);
				}
				if(desY >= 17) {
					setSwitch(19,19,17,19);
				}
				else {
					setSwitch(19,19,desY-1,desY+1);
				}
			}
			else if(desY == 0) {
				setSwitch(desX-1,desX+1,0,0);
			}
			else if(desY == 19) {
				setSwitch(desX-1,desX+1,19,19);
			}
			return true;
		}
		return false;
	}
	
	public void setSwitch(int xStart, int xEnd, int yStart, int yEnd) {
		for(int i = switchXL; i <= switchXR; i++ ) {
			for(int j = switchYL; j <= switchYR; j++) {
				board[i][j] = null;
			}
		}
		for(int i = xStart; i <= xEnd; i++ ) {
			for(int j = yStart; j <= yEnd; j++) {
				board[i][j] = new Components(i,j,"Switch");
			}
		}
		switchXL = xStart;
		switchXR = xEnd;
		switchYL = yStart;
		switchYR = yEnd;
	}
	
	//add bomb to the board
	
	public void addBomb(int x, int y) {
		board[x][y] = new Components(x,y,"Bomb");
		bombX = x;
		bombY = y;
	}
	
	//remove bomb to the board
	
	public void removeBomb() {
		board[bombX][bombY] = null;
		bombX = 0;
		bombY = 0;
	}
	
	//add star to the board
	
	public void addStar(int x, int y) {
		board[x][y] = new Components(x,y,"Star");
		starX = x;
		starY = y;
	}
	
	//remove star to the board
	
	public void removeStar() {
		board[starX][starY] = null;
		starX = 0;
		starY = 0;
	}
	
	//add capB to the board
	
	public void addCapB(int x, int y) {
		board[x][y] = new Components(x,y,"CapsuleB");
		capBX = x;
		capBY = y;
	}
	
	//remove capB to the board
	
	public void removeCapB() {
		board[capBX][capBY] = null;
		capBX = 0;
		capBY = 0;
	}
	
	//add capS to the board
	
	public void addCapS(int x, int y) {
		board[x][y] = new Components(x,y,"CapsuleS");
		capSX = x;
		capSY = y;
	}
	
	//remove capS to the board
	
	public void removeCapS() {
		board[capSX][capSY] = null;
		capSX = 0;
		capSY = 0;
	}
	
	//add timer to the board
	
	public void addTimer(int x, int y) {
		board[x][y] = new Components(x,y,"Timer");
		timerX = x;
		timerY = y;
	}
		
	//remove timer to the board
		
	public void removeTimer() {
		board[timerX][timerY] = null;
		timerX = 0;
		timerY = 0;
	}
	
	//move the ball to destination position and return if it is successful
	
	public void moveBall(int desX, int desY) {
		board[ballX][ballY] = null;
		board[desX][desY] = new Components(desX,desY,"Ball");
		ballX = desX;
		ballY = desY;
	}
	
	//check of the ball is hitting the switch
	public boolean isHit() {
    	int desX = ballX + (direction/Math.abs(direction));
    	int desY = ballY - direction;
		if((desX <= 0) && (desY>=switchYL) && (desY<=switchYR) && ((switchXR == 0) && (switchXL == 0))) return true;
		if((desX <= 0) && (desX>=switchXL) && (desX<=switchXR) && ((switchYR == 0) && (switchYL == 0) && (desY<=0))) return true;
		if((desX <= 0) && (desX>=switchXL) && (desX<=switchXR) && ((switchYR == 19) && (switchYL == 19) && (desY>=19))) return true;
		if((desX >= 19) && (desY>=switchYL) && (desY<=switchYR) && ((switchXR == 19) && (switchXL == 19))) return true;
		if((desX >= 19) && (desX>=switchXL) && (desX<=switchXR) && ((switchYR == 0) && (switchYL == 0) && (desY<=0))) return true;
		if((desX >= 19) && (desX>=switchXL) && (desX<=switchXR) && ((switchYR == 19) && (switchYL == 19) && (desY>=19))) return true;
		if((desY <= 0) && (desX>=switchXL) && (desX<=switchXR) && ((switchYR == 0) && (switchYL == 0))) return true;
		if((desY <= 0) && (desY>=switchYL) && (desY<=switchYR) && ((switchXR == 0) && (switchXL == 0) && (desX<=0))) return true;
		if((desY <= 0) && (desY>=switchYL) && (desY<=switchYR) && ((switchXR == 19) && (switchXL == 19) && (desX>=19))) return true;
		if((desY >= 19) && (desX>=switchXL) && (desX<=switchXR) && ((switchYR == 19) && (switchYL == 19))) return true;
		if((desY >= 19) && (desY>=switchYL) && (desY<=switchYR) && ((switchXR == 0) && (switchXL == 0) && (desX<=0))) return true;
		if((desY >= 19) && (desY>=switchYL) && (desY<=switchYR) && ((switchXR == 19) && (switchXL == 19) && (desX>=19))) return true;
		return false;
	}
	
	//change direction of the ball
	public void changeDir() {
		Random rand = new Random();
		if(direction > 0) {
			direction = - rand.nextInt(4);
			if(direction == 0) direction = -1;
		}
		else {
			direction = rand.nextInt(4);
			if(direction == 0) direction = 1;
		}
	}
	
	//get the components in the corresponding coordinates
	
	public Components getComponent(int x, int y) {
		return board[x][y];
	}
	
	//check if the player loses by 
	//(1) if the ball goes out of the board 
	//(2) if the ball gits the bomb
	
	public boolean doesLose() {
		if(ballX < 0 || ballX > 19 || ballY < 0 || ballY > 19) return true;
		if(ballX==bombX && ballY==bombY) return true;
		return false;
	}
	
	//check if the ball encounter the star
	
	public boolean doesScore() {
		if(ballX==starX && ballY==starY) return true;
		return false;
	}
	
	//check if the ball encounter the capsuleBig
	
	public boolean isBigger() {
		if(ballX==capBX && ballY==capBY) {
			isSmall = 0;
			isBig = 1;
			return true;
		}
		return false;
	}
	
	//check if the ball encounter the capsuleSmall
	
	public boolean isSmaller() {
		if(ballX==capSX && ballY==capSY) {
			isBig = 0;
			isSmall = 1;
			return true;
		}
		return false;
	}
	
	//check if the ball encounter the timer
	
	public boolean incTime() {
		if(ballX==timerX && ballY==timerY) return true;
		return false;
	}
		
	//check if the ball encounter the capsuleBig
		
	public boolean isBig() {
		return isBig == 1;
	}
		
	//check if the ball encounter the capsuleSmall
		
	public boolean isSmall() {
		return isSmall == 1;
	}
	
	//make the ball to normal size
	
	public void sizeNormal() {
		isBig = 0;
		isSmall = 0;
	}

}
