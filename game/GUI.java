package gui;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import java.util.ArrayList;
import javafx.util.Pair; 
import java.util.List;
import java.applet.*;
import java.text.NumberFormat;
import java.net.*;
import game.Platform;

public class GUI {
    private static final int HEIGHT = 20;
    private static final int WIDTH = 20;
    private static final int INCREMENT = 3;
    private int stop;
    
	private final JPanel gui = new JPanel(new BorderLayout(3, 3));
    private final JFrame frame = new JFrame("Bouncing Ball");
    private JButton[][] tiles = new JButton[WIDTH][HEIGHT];
    public Timer timer = null;
    
    private JPanel platform;
    private final JLabel message = new JLabel("Click New button to start new game!");
    private final Color color = new Color(118,150,86);
    private final Color highlight = new Color(255,165,0);
    
    private JLabel name;
    private JLabel score;
    
    private JButton restart;
    private JButton newGame;
    
    private JLabel timecount;
    private long remaining;
    private long lastUpdate;
    NumberFormat format;
    


    public GUI() {
    	remaining = 60000; //1min
    	stop = 0;
        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        gui.add(tools, BorderLayout.PAGE_START);
        
        format = NumberFormat.getNumberInstance();
        format.setMinimumIntegerDigits(2);
        
		timecount = new JLabel();
		timecount.setHorizontalAlignment(SwingConstants.CENTER );
		timecount.setOpaque(true);
		
        if (remaining < 0) remaining = 0;
        int minutes = (int)(remaining/60000);
        int seconds = (int)(((remaining)/1000)) - minutes * 60;
        timecount.setText(format.format(minutes) + ":" + format.format(seconds));
      
        newGame = new JButton("New");
        newGame.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                timer.start();
            }
        });
        
        restart = new JButton("Restart");
        restart.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                timer.restart();
                remaining = 60000;
            }
        });
        
    	timer = new Timer(1000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateDisplay();
            }
        });


        tools.add(newGame);
        tools.add(restart);
        tools.add(timecount);
        
        tools.addSeparator();
        tools.add(message);


        platform = new JPanel(new GridLayout(0, 20)) ;
        platform.setBackground(color);
        platform.setBorder(new LineBorder(Color.BLACK));
        
 
        score=new JLabel("0",SwingConstants.CENTER);
        platform.add(new JLabel("Score",SwingConstants.CENTER));
        platform.add(score);
        for(int i = 0; i<18; i++) {
        	platform.add(new JLabel(""));
        }
        gui.add(platform);

        // create the squares
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
            	JButton b = new JButton();
                ImageIcon icon = new ImageIcon(
                        new BufferedImage(22, 22, BufferedImage.TYPE_INT_ARGB));
                
                b.setIcon(icon);
                b.setOpaque(true);
                b.setBorderPainted(false);
                b.setBackground(new Color(255,255,255));	
                tiles[j][i] = b;
            }
        }
        
        // create the platform
           
       
      for (int ii = 0; ii < HEIGHT; ii++) {
           for (int jj = 0; jj < WIDTH; jj++) {
               	platform.add(tiles[jj][ii]);
           }
       }

       frame.add(gui);
       
       frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
       frame.setLocationByPlatform(true);

       frame.pack();
       frame.setMinimumSize(frame.getSize());
       frame.setVisible(true);
    }
    

    void updateDisplay() {
        //long now = System.currentTimeMillis();
        //long elapsed = now - lastUpdate;
        remaining -= 1000;
        //lastUpdate = now;

        if (remaining < 0) remaining = 0;
        int minutes = (int)(remaining/60000);
        int seconds = (int)((remaining - minutes*60000)/1000);
        timecount.setText(format.format(minutes) + ":" + format.format(seconds));

        if (remaining == 0) {
            timer.stop();
			stop = 1;
        }
    }
    
    public void addTime(int time) {
    	remaining+=time;
    	updateDisplay();
    }

    public void timeResume() {
        // Restore the time we're counting down from and restart the timer.
        lastUpdate = System.currentTimeMillis();
        timer.start();
    }

    public void timePause() {
        // Subtract elapsed time from the remaining time and stop timing
        long now = System.currentTimeMillis();
        remaining -= (now - lastUpdate);
        timer.stop();
    }
    
    
    public final JComponent getPlatform() {
        return platform;
    }

    public final JComponent getGui() {
        return gui;
    }
    
    public int isStopped() {
    	return stop;
    }

    
    //setup

    public final void newGame() {
    	remaining = 60000; //1min
    	stop = 0;
    	for(int i=0; i<WIDTH;i++){
    		for(int j = 0; j<HEIGHT;j++){
    			tiles[i][j].setIcon(null);
    		}
    	}
        message.setText("Game starts!");
    
        //setup images for ball
    	Image Ball = new ImageIcon(this.getClass().getResource("/img/ball.png")).getImage();
    	tiles[18][1].setIcon(new ImageIcon(Ball));
        
    	//setup images for switch
    	Image Swi = new ImageIcon(this.getClass().getResource("/img/switch.png")).getImage();
    	tiles[0][19].setIcon(new ImageIcon(Swi));
    	tiles[1][19].setIcon(new ImageIcon(Swi));
    	tiles[2][19].setIcon(new ImageIcon(Swi));
    	
        //setup images for bomb
    	Image Bomb = new ImageIcon(this.getClass().getResource("/img/bomb.png")).getImage();
    	tiles[13][4].setIcon(new ImageIcon(Bomb));
    	
    	//setup images for timer
    	Image Timerr = new ImageIcon(this.getClass().getResource("/img/timer.png")).getImage();
    	tiles[17][2].setIcon(new ImageIcon(Timerr));
    	
    	//setup images for capB
    	Image capB = new ImageIcon(this.getClass().getResource("/img/capsuleb.png")).getImage();
    	tiles[15][4].setIcon(new ImageIcon(capB));
    	
    	//setup images for capB
    	Image capS = new ImageIcon(this.getClass().getResource("/img/capsules.png")).getImage();
    	tiles[4][15].setIcon(new ImageIcon(capS));
    	
        //setup images for star
    	Image Star = new ImageIcon(this.getClass().getResource("/img/star.png")).getImage();
    	tiles[15][9].setIcon(new ImageIcon(Star));
    }
    
    public void setScore(int s){
    	score.setText(String.valueOf(s));
    }
    
    
    public void setMessage(String message){
    	this.message.setText(message);
    }
    
    public JFrame getFrame(){
    	return this.frame;
    }
    
    public JButton getTile(int x, int y){
    	return tiles[x][y];
    }
    
    public void highlight(JButton in){
    	in.setBackground(highlight);
    }
    
    public void dehighlight(JButton B, int x){
    	B.setBackground(color);	
    }
    
    /**
   	 * This function will update image on the given position
   	 *
   	 * @param Board board the game board
   	 * @param int x updating x position
   	 * @param int y updating x position
   	**/
    public void updateImage(Platform platform, int x, int y){
    	if(platform.getComponent(x, y) == null){
    		tiles[x][y].setIcon(null);
    		return;
    	}
    	tiles[x][y].setBackground(new Color(255,255,255));
    	String type = platform.getComponent(x, y).getType();
    	Image input = null;
    	if(type == "Ball" && platform.isBig()) {
    		System.out.println("turning big");
    		input = new ImageIcon(this.getClass().getResource("/img/ballb.png")).getImage();
    	}
    	else if(type == "Ball" && platform.isSmall()) {
    		System.out.println("turning small");
    		input = new ImageIcon(this.getClass().getResource("/img/balls.png")).getImage();
    	}
    	else {
    		input = new ImageIcon(this.getClass().getResource("/img/"+type.toLowerCase()+".png")).getImage();
    	}
    	tiles[x][y].setIcon(new ImageIcon(input));
    }
    
    
}
