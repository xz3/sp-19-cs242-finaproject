package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Game.*;

public class StructureTest {
	
	@Test
	public void testComponentConstructor() {
		Ball ball = new Ball(4,1);
		assertEquals(ball.getType(),"Ball");
		Bomb b = new Bomb(2,1);
		assertEquals(b.getType(),"Bomb");
		Switch s = new Switch(2,2);
		assertEquals(s.getType(),"Switch");
	}
	
	@Test
	public void testInitialPosition() {
		Platform p = new Platform();
		Components res = p.getComponent(495, 5);
		assertEquals(res.getType(),"Ball");
		res = p.getComponent(5, 495);
		assertEquals(res.getType(),"Switch");
	}
	
	@Test
	public void testBombSetUp() {
		Platform p = new Platform();
		p.addComponent(34, 5, "Bomb");
		Components res = p.getComponent(34, 5);
		assertEquals(res.getType(),"Bomb");
	}

}

