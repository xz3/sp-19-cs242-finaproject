package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Game.*;

public class BallHitsSwitchTest {
	
	@Test
	public void testBallHitsSwitch() {
		Platform p = new Platform();
		p.addComponent(5, 5, "Ball");
		p.addComponent(5, 5, "Switch");
		boolean out = p.isHit();
		assertEquals(out,true);
	}
	
	@Test
	public void testNotBallHitsSwitch() {
		Platform p = new Platform();
		p.addComponent(85, 85, "Ball");
		p.addComponent(5, 5, "Switch");
		boolean out = p.isHit();
		assertEquals(out,false);
	}

}