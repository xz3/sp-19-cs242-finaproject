package Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import Game.*;

public class EndConditionTest {
	
	@Test
	public void testOutOfBound() {
		Platform p = new Platform();
		p.addComponent(0, 0, "Ball");
		boolean out = p.doesLose();
		assertEquals(out,true);
	}
	
	@Test
	public void testNotOutOfBound() {
		Platform p = new Platform();
		p.addComponent(85, 85, "Ball");
		boolean out = p.doesLose();
		assertEquals(out,false);
	}
	
	@Test
	public void testHitingBomb() {
		Platform p = new Platform();
		p.addComponent(34, 5, "Ball");
		p.addComponent(34, 5, "Bomb");
		boolean out = p.doesLose();
		assertEquals(out,true);	
	}
	
	@Test
	public void testNotHitingBomb() {
		Platform p = new Platform();
		p.addComponent(34, 5, "Ball");
		p.addComponent(89, 5, "Bomb");
		boolean out = p.doesLose();
		assertEquals(out,false);	
	}

}

